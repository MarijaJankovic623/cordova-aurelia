var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
define(["require", "exports", "aurelia-dependency-injection", "aurelia-router"], function (require, exports, aurelia_dependency_injection_1, aurelia_router_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var RestaurantMenu = (function () {
        function RestaurantMenu(router) {
            this.router = router;
        }
        RestaurantMenu.prototype.attached = function () {
            this.dishes = [{ name: 'Lazanje' }, { name: 'Cezar salata' }, { name: 'Cokoladna torta' }];
        };
        RestaurantMenu.prototype.leaveComment = function () {
            this.router.navigateToRoute('comment');
        };
        RestaurantMenu.prototype.back = function () {
            this.router.navigateToRoute('restaurants-list');
        };
        RestaurantMenu = __decorate([
            aurelia_dependency_injection_1.autoinject(),
            __metadata("design:paramtypes", [aurelia_router_1.Router])
        ], RestaurantMenu);
        return RestaurantMenu;
    }());
    exports.RestaurantMenu = RestaurantMenu;
});

//# sourceMappingURL=restaurant-menu.js.map
