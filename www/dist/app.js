define(["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var App = (function () {
        function App() {
        }
        App.prototype.configureRouter = function (config, router) {
            config.title = 'Aurelia';
            config.map([
                {
                    route: ['', 'restaurants-list'],
                    name: 'restaurants-list',
                    moduleId: 'pages/restaurants-list/restaurants-list',
                    nav: true,
                    title: 'Restorani'
                },
                {
                    route: ['details', 'restaurant-details'],
                    name: 'details',
                    moduleId: 'pages/restaurant-details/restaurant-details',
                    nav: false,
                    title: 'Detaljno o restoranu'
                },
                {
                    route: ['menu', 'restaurant-menu'],
                    name: 'menu',
                    moduleId: 'pages/restaurant-menu/restaurant-menu',
                    nav: false,
                    title: 'Jelovnik'
                },
                { route: ['login', 'login'], name: 'login', moduleId: 'pages/login/login', nav: true, title: 'Prijava' },
                {
                    route: ['registration', 'registration'],
                    name: 'registration',
                    moduleId: 'pages/registration/registration',
                    nav: true,
                    title: 'Registracija'
                },
                {
                    route: ['orders', 'orders'],
                    name: 'orders',
                    moduleId: 'pages/orders/orders',
                    nav: true,
                    title: 'Porudzbine'
                },
                {
                    route: ['order-details', 'order-details'],
                    name: 'order-details',
                    moduleId: 'pages/order-details/order-details',
                    nav: false,
                    title: 'Detalji porudzbine'
                },
                {
                    route: ['change', 'change'],
                    name: 'change',
                    moduleId: 'pages/data-change/data-change',
                    nav: true,
                    title: 'Izmeni'
                },
                {
                    route: ['comment', 'comment'],
                    name: 'comment',
                    moduleId: 'pages/comment/comment',
                    nav: false,
                    title: 'Ostavi komentar'
                },
            ]);
            this.router = router;
        };
        return App;
    }());
    exports.App = App;
});

//# sourceMappingURL=app.js.map
